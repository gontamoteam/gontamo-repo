#include "HelloWorldScene.h"
#import "SimpleAudioEngine.h"

USING_NS_CC;

CCScene* HelloWorld::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    HelloWorld *layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }
    //BGM
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("bgm1.mp3",true);

    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    CCMenuItemImage *pCloseItem = CCMenuItemImage::create(
                                        "CloseNormal.png",
                                        "CloseSelected.png",
                                        this,
                                        menu_selector(HelloWorld::menuCloseCallback));
    
	pCloseItem->setPosition(ccp(origin.x + visibleSize.width - pCloseItem->getContentSize().width/2 ,
                                origin.y + pCloseItem->getContentSize().height/2));

    // create menu, it's an autorelease object
    CCMenu* pMenu = CCMenu::create(pCloseItem, NULL);
    pMenu->setPosition(CCPointZero);
    this->addChild(pMenu, 1);

    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label
    
    CCLabelTTF* pLabel = CCLabelTTF::create("ねぎを相手のゴールにシュゥゥゥーッ！！超！エキサイティン！！", "Arial", 24);
    
    // position the label on the center of the screen
    pLabel->setPosition(ccp(origin.x + visibleSize.width/2,
                            origin.y + visibleSize.height - pLabel->getContentSize().height));

    // add the label as a child to this layer
    this->addChild(pLabel, 1);
    
//negi
    // add "HelloWorld" splash screen"
    CCSprite* negi = CCSprite::create("negi.png");
    // position the sprite on the center of the screen
    negi->setPosition(ccp(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    negi->setTag(1);
    // add the sprite as a child to this layer
    this->addChild(negi, 0);
    CCAction* action = CCScaleBy::create(2, 0.5);
    negi->runAction(action);
    CCAction* rotate = CCRotateBy::create(2, 1320);
    negi->runAction(rotate);

//tamanegi
    CCSprite* tamanegi = CCSprite::create("onion.png");
    tamanegi->setPosition(ccp(visibleSize.width*0.9, visibleSize.height/2 + origin.y));
    CCAction* tamanegirotate = CCRotateBy::create(10, 36000);
    tamanegi->runAction(tamanegirotate);
    CCAction* tamanegiaction = CCScaleBy::create(10, 0.2);
    tamanegi->runAction(tamanegiaction);
    this->addChild(tamanegi, 0);

//wagiri
    CCSprite* wagiri = CCSprite::create("wagiri.png",CCRectMake(0, 0, 100, 135));
    wagiri->setPosition(ccp(visibleSize.width*0.8, visibleSize.height/2 + origin.y));
    CCAction* wagirirotate = CCRotateBy::create(20, 36000);
    wagiri->runAction(wagirirotate);
    this->addChild(wagiri);
    
//タッチイベント
    this->setTouchMode(kCCTouchesAllAtOnce);
    this->setTouchEnabled(true);

    // レシピ04で追加
    // 〇〇秒に一度、gameLogic()を実行させる
    this->schedule(schedule_selector(HelloWorld::gameLogic), 1.0);
    
// レシピ08で追加
    // 毎フレームでupdate()を実行させる
    this->scheduleUpdate();
// レシピ09補足：上記は以下と同じ結果
    //this->schedule(schedule_selector(Recipe03to16::update));
    
// ▼ レシピ12で追加 //////////
    // SCORE 文字列
    CCLabelTTF* scoreLabel = CCLabelTTF::create("長ねぎの傷み", "arial", 48);
    scoreLabel->setPosition(ccp(visibleSize.width/2, visibleSize.height/9));
    scoreLabel->setColor(ccc3(255, 0, 127));
    scoreLabel->setTag(10);
    this->addChild(scoreLabel);
    
    // スコアポイント
    CCLabelTTF* pointsLabel = CCLabelTTF::create("0", "arial", 48);
    pointsLabel->setPosition(ccp(scoreLabel->getPositionX() + scoreLabel->getContentSize().width,
                                 visibleSize.height/9));
    pointsLabel->setColor(ccc3(255, 0, 127));
    pointsLabel->setTag(11);
    this->addChild(pointsLabel);
// ▲ レシピ12で追加 \\\\\\\\\\

    return true;
}


void HelloWorld::menuCloseCallback(CCObject* pSender)
{
    CCDirector::sharedDirector()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void HelloWorld::ccTouchesBegan(cocos2d::CCSet* touches,cocos2d::CCEvent* event)
{    
    // タッチされた座標を取得
    CCTouch *touch = (CCTouch *)touches->anyObject();
    CCPoint location = touch->getLocationInView();
    location = CCDirector::sharedDirector()->convertToGL(location);
    
    // 輪切り取得（tag=1で識別）
    CCSprite *Sprite = (CCSprite *)this->getChildByTag(1);
    
    // 輪切りをタッチされた座標まで移動させる
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    // 継続時間は移動距離に比例させる
    float length = sqrtf(powf(location.x - Sprite->getPositionX(), 2.0f) + powf(location.y - Sprite->getPositionY(), 2.0f));
    float duration = length / winSize.width * 1.5f;
    CCMoveTo* actionMove = CCMoveTo::create(duration, location);
    Sprite->runAction(actionMove);
}

// ▼ レシピ04で追加 //////////
void HelloWorld::addFood()
{
    // タマネギのスプライトを作成してレイヤへ追加
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    CCSprite* food = CCSprite::create("onion.png", CCRectMake(0, 0, 60, 60) );
    int height = rand() % (int)winSize.height;
    food->setPosition(ccp(winSize.width + food->getContentSize().width/2, height));
    // 後で識別できるようにタグを設定
    food->setTag(2);
    this->addChild(food);
    
    // アクションの継続時間
    float duration = 2.0f;
    // アクションの作成
    CCMoveTo* actionMove = CCMoveTo::create(duration,
                                            ccp(food->getContentSize().width*3/2, food->getPositionY()));
    
    // レシピ05で追加
    // spriteMoveFinished()メソッドを呼び出すCCCallFuncNアクションを作成
    CCCallFuncN* actionMoveDone =
    CCCallFuncN::create( this, callfuncN_selector(HelloWorld::spriteMoveFinished));
    
    // アクションの実行
    //food->runAction(actionMove);
    // レシピ05で変更
    // actionMoveアクションの後に、actionMoveDoneを実行させる
    food->runAction(CCSequence::create(actionMove, actionMoveDone, NULL));
    
    
    // ▼ レシピ13で追加 //////////
    CCActionInterval* action1;
    CCActionInterval* action2;
    // ベジェ曲線移動の際に利用
    ccBezierConfig bezier;
    bezier.controlPoint_1 = ccp(winSize.width, winSize.height);
    bezier.controlPoint_2 = ccp(winSize.width/2, -winSize.height);
    bezier.endPosition = ccp(0, winSize.height);
    
    // アクションの種類を切り替えて実行
    switch (this->m_actionType) {
        case 1:
            // 拡大：４倍に拡大
            action1 = CCScaleTo::create(duration, 4.0f);
            food->runAction(action1);
            break;
        case 2:
            // 回転：360度回転
            action1 = CCRotateBy::create(duration, 360);
            food->runAction(action1);
            action2 = CCRepeatForever::create(action1);
            break;
        case 3:
            // 拡大と回転の組み合わせ
            action1 = CCScaleTo::create(duration, 4.0f);
            action2 = CCRotateBy::create(duration, 360);
            //food->runAction(action1);
            //food->runAction(action2);
            food->runAction(CCSpawn::create(action1, action2, NULL));
            break;
        case 4:
            // 永久繰り返し
            action1 = CCRotateBy::create(0.5f, -360);
            // 0.5秒で1回転を永久に繰り返す（削除されるまで）
            action2 = CCRepeatForever::create(action1);
            food->runAction(action2);
            break;
        case 5:
            // ジャンプ：高さ100、回数5
            action1 = CCJumpBy::create(duration, ccp(0, 0), 100, 5);
            food->runAction(action1);
            break;
        case 6:
            // シーケンス実行：４倍に拡大した後、３回ジャンプ
            action1 = CCScaleTo::create(duration/2.0f, 4.0f);
            action2 = CCJumpBy::create(duration/2.0f, ccp(0, 0), 100, 3);
            food->runAction(CCSequence::create(action1, action2, NULL));
            break;
        case 7:
            // フェードイン／アウト
            // 半分の時間でフェードイン
            action1 = CCFadeIn::create(duration/2.0f);
            // フェードインの反転
            action2 = action1->reverse();
            // フェードインの反転は以下と同じ
            //action2 = CCFadeOut::create(duration/2.0f);
            // action1、action2 の順に実行
            food->runAction(CCSequence::create(action1, action2, NULL));
            break;
        case 8:
            // 水平移動のアクションを停止
            food->stopAllActions();
            // ベジェ曲線に沿って移動
            action1 = CCBezierTo::create(duration, bezier);
            food->runAction(CCSequence::create(action1, actionMoveDone, NULL));
            break;
            
            
            // ▼ レシピ14で追加 //////////
        case 9:
            // 水平移動のアクションを停止
            food->stopAllActions();
            // EaseInOutアクションを作成
            action1 = CCEaseInOut::create((CCActionInterval *)actionMove, 3.0f);
            food->runAction(CCSequence::create(action1, actionMoveDone, NULL));
            break;
        case 10:
            // 水平移動のアクションを停止
            food->stopAllActions();
            // CCEaseBounceOutアクションを作成
            action1 = CCEaseBounceOut::create((CCActionInterval *)actionMove);
            food->runAction(CCSequence::create(action1, actionMoveDone, NULL));
            break;
        case 11:
            // 水平移動のアクションを停止
            food->stopAllActions();
            // CCEaseBounceOutアクションを作成
            action1 = CCEaseBounceOut::create((CCActionInterval *)actionMove);
            food->runAction(CCSequence::create(action1, actionMoveDone, NULL));
            // ジャンプ：高さ100、回数5
            action2 = CCJumpBy::create(duration, ccp(0, 0), 100, 5);
            // ジャンプも同時に実行
            food->runAction(action2);
            break;
            // ▲ レシピ14で追加 \\\\\\\\\\
            
            
        default:
            // 水平移動
            break;
    }
    CCLOG("m_actionType :%d", this->m_actionType);
    // actionTypeをインクリメント（8の次は0へ戻る）
    //this->m_actionType = (this->m_actionType + 1)%9;
    // レシピ14で変更
    // actionTypeをインクリメント（11の次は0へ戻る）
    this->m_actionType = (this->m_actionType + 1)%12;
    // ▲ レシピ13で追加 \\\\\\\\\\

}
// ▲ レシピ04で追加 \\\\\\\\\\

// ▼ レシピ04で追加 //////////
void HelloWorld::gameLogic()
{
    this->addFood();
}
// ▲ レシピ04で追加 \\\\\\\\\\

// ▼ レシピ05で追加 //////////
void HelloWorld::spriteMoveFinished(CCNode* sender)
{
    // 終了したアクションのスプライトを取得（本メソッドの呼び出し元を取得）
    CCSprite *sprite = (CCSprite *)sender;
    bool isCleanUp = true;
    // スプライトをレイヤから削除して破棄する
    this->removeChild(sprite, isCleanUp);
    
    //sprite->setVisible(false);
    //sprite->setOpacity(0);
}
// ▲ レシピ05で追加 \\\\\\\\\\

// ▼ レシピ08で追加 //////////
void HelloWorld::update(float dt)
{
    // ねぎのスプライトを取得（tag=1で識別）
    CCSprite* player = (CCSprite *)this->getChildByTag(1);
    CCRect playerRect = CCRectMake(
                                   player->getPosition().x - (player->getContentSize().width/4),
                                   player->getPosition().y - (player->getContentSize().height/4),
                                   player->getContentSize().width/2,
                                   player->getContentSize().height/2);
    
    // タマネギのスプライトを取得（tag=2で識別）
    CCSprite* food = (CCSprite *)this->getChildByTag(2);
    if (food == NULL) return; // foodが作成されていなければ処理を抜ける
    CCRect foodRect = CCRectMake(
                                 food->getPosition().x - (food->getContentSize().width/2),
                                 food->getPosition().y - (food->getContentSize().height/2),
                                 food->getContentSize().width,
                                 food->getContentSize().height);
    
    // 衝突判定
    //if (playerRect.intersectsRect(foodRect))
    // レシピ13で変更
    if (playerRect.intersectsRect(foodRect) && player->isVisible())
    {
        // タマネギを削除
        this->removeChild(food, true);
        
        // レシピ09で追加
        // ねぎのイメージ変更
        player->setTexture(CCTextureCache::sharedTextureCache()->addImage("negi.png"));
        // 0.1秒後にRecipe03to16::eat()を１度だけ実行させる
        this->scheduleOnce(schedule_selector(HelloWorld::eat), 0.1f);
    }
}
// ▲ レシピ08で追加 \\\\\\\\\\

// ▼ レシピ09で追加 //////////
void HelloWorld::eat()
{
    // ねぎのイメージを戻す
    CCSprite* player = (CCSprite *)this->getChildByTag(1);
    player->setTexture(CCTextureCache::sharedTextureCache()->addImage("negi.png"));
    
    
    // ▼ レシピ16で追加 //////////
    // フレームアニメーション
    // コーディングしてCCAnimationを作成
    CCAnimation* animation = CCAnimation::create();
    for (int i = 3; i <= 5; i++)
    {
        char szName[100] = {0};
        sprintf(szName, "negi%02d.png", i);
        // フレーム毎の画像を追加
        animation->addSpriteFrameWithFileName(szName);
    }
    // ３フレームで150ミリ秒継続
    animation->setDelayPerUnit(0.15f / 3.0f);
    // アニメーション終了後に元の画像に戻す
    animation->setRestoreOriginalFrame(true);
    // ３回繰り返す
    animation->setLoops(3);
    
    // plistからCCAnimationを作成
    CCAnimationCache* cache = CCAnimationCache::sharedAnimationCache();
    cache->addAnimationsWithFile("animations.plist");
    // plist内の"bite"という名前のアニメーションを生成
    //CCAnimation* animation2 = cache->animationByName("bite");
    
    // フレームアニメーションを実行
    CCAnimate* action = CCAnimate::create(animation);
    //CCAnimate* action = CCAnimate::create(animation2);
    player->runAction(action);
    // ▲ レシピ16で追加 \\\\\\\\\\
    
    
    // ▼ レシピ12で追加 //////////
    // スコアポイントを加算
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    this->m_points += player->getPositionX()/(winSize.width/4) + 1;
    // スコアポイントのラベルを取得
    CCLabelTTF* label = (CCLabelTTF *)this->getChildByTag(11);
    // intからCCStringに変換
    CCString* points = CCString::createWithFormat("%d", this->m_points);
    // スコアポイントの表示を更新
    label->setString(points->getCString());
    // ▲ レシピ12で追加 \\\\\\\\\\
    
}
// ▲ レシピ09で追加 \\\\\\\\\\