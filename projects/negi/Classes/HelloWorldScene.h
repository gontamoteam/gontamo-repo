#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"

class HelloWorld : public cocos2d::CCLayer
{
protected:
    // レシピ12で追加
    int m_points;
    
    // レシピ13で追加
    int m_actionType;
    
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::CCScene* scene();
    
    // a selector callback
    void menuCloseCallback(CCObject* pSender);
    
    // implement the "static node()" method manually
    CREATE_FUNC(HelloWorld);
    
    void ccTouchesBegan(cocos2d::CCSet* touches,cocos2d::CCEvent* event);
    
    void addFood();
    void gameLogic();
    
    // レシピ05で追加
    void spriteMoveFinished(CCNode* sender);
    
    // レシピ08で追加
    void update(float dt);
    
    // レシピ09で追加
    void eat();
    
    // レシピ11で追加
    void changeMode(CCObject* sender);
};

#endif // __HELLOWORLD_SCENE_H__
