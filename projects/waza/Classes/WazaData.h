//
//  WazaData.h
//  waza
//
//  Created by gontamo on 13/09/01.
//
//

#ifndef __waza__WazaData__
#define __waza__WazaData__

#include "cocos2d.h"

using namespace cocos2d;

class WazaData : public cocos2d::CCNode
{
public:
    WazaData();
    virtual ~WazaData();
    
    CCString *mName;
    CCString *mExplain;
};

#endif /* defined(__waza__WazaData__) */
