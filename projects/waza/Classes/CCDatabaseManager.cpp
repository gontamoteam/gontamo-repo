//
//  CCDatabaseManager.cpp
//  waza
//
//  Created by ugabugab on 9/1/13.
//
//

#include "CCDatabaseManager.h"
#include <sqlite3.h>
#include "WazaData.h"

#define DB_FILE_NAME "waza.db"

//static CCDatabaseManager *sDatabaseManager = NULL;
CCDatabaseManager *CCDatabaseManager::sDatabaseManager = NULL; // singleton instance

/**
 * constructor
 */
CCDatabaseManager::CCDatabaseManager()
{

}

/**
 * destructor
 */
CCDatabaseManager::~CCDatabaseManager()
{
    
}

/**
 * initialize
 */
void CCDatabaseManager::initialize()
{
    // TODO: initialize
}

/**
 * terminate
 */
void CCDatabaseManager::terminate()
{
    // TODO: terminate
}

/**
 * getting file path of Database
 * @returns path
 */
CCString *CCDatabaseManager::getDatabaseFilePath()
{
    std::string path = CCFileUtils::sharedFileUtils()->fullPathForFilename(DB_FILE_NAME);
    CCString *pathString = new CCString(path);
    
    return (CCString *)pathString->autorelease();  // reutrns with autorelese
}

/**
 * selecting all "WazaData" list
 * @returns WazaData list
 */
CCArray *CCDatabaseManager::selectAllWazaDatas()
{
    CCArray *retArray = new CCArray();
    
    sqlite3 *db = NULL;
    if (sqlite3_open(this->getDatabaseFilePath()->m_sString.c_str(), &db) == SQLITE_OK)
    {
        CCString *sqlStr = new CCString("SELECT * FROM tblWaza;");
        
        sqlite3_stmt *q = NULL;
        if (sqlite3_prepare (db, sqlStr->m_sString.c_str(), -1, &q, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(q) == SQLITE_ROW)
            {
                int ID = sqlite3_column_int(q, 0);
                const unsigned char *name = sqlite3_column_text(q, 1);
                const unsigned char *explatination  = sqlite3_column_text(q, 2);
                
                CCLOG("%s : %s", name, explatination);
            }
        }
        sqlite3_finalize(q);
    }
    CCLOG("%s", sqlite3_errmsg(db));
    sqlite3_close(db);
    
    return (CCArray *)retArray->autorelease();
}
