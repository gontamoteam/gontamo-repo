//
//  library.h
//  waza
//
//  Created by gontamo on 13/07/24.
//
//

#ifndef __waza__library__
#define __waza__library__

#include "cocos2d.h"
#include "WazaData.h"

class library : public cocos2d::CCLayer
{
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::CCScene* scene();
    static cocos2d::CCScene* scene(WazaData *data);
    
    // a selector callback
    void menuCloseCallback(CCObject* pSender);
    
    // implement the "static node()" method manually
    CREATE_FUNC(library);
    
    virtual bool ccTouchBegan(cocos2d::CCTouch* pTouch, cocos2d::CCEvent* pEvent);
    virtual void ccTouchEnded(cocos2d::CCTouch* pTouch, cocos2d::CCEvent* pEvent);

    //背景
    cocos2d::CCSprite* m_background;
    void showBackground();
    
    //変数初期化
    void initForVariables();
    
    //ボタン
    void LibraryButton();
    void LibraryButton2Pressed();
    
    void EternalButton();
    void MukaButton();
    void GekiButton();
    void RoyalButton();
    void EndButton();
    void JetButton();
    void FinalButton();
    void SpecialButton();
    void WinningButton();
    void SkyButton();
    void MarbelousButton();
    void HaiokuButton();
    void LaButton();
    void HyperButton();
    void CrossButton();
    void UniversalButton();
    
    void EternalButtonPressed();
    void MukaButtonPressed();
    void GekiButtonPressed();
    void RoyalButtonPressed();
    void EndButtonPressed();
    void JetButtonPressed();
    void FinalButtonPressed();
    void SpecialButtonPressed();
    void WinningButtonPressed();
    void SkyButtonPressed();
    void MarbelousButtonPressed();
    void HaiokuButtonPressed();
    void LaButtonPressed();
    void HyperButtonPressed();
    void CrossButtonPressed();
    void UniversalButtonPressed();
    
    int ButtonNo;
    void ExplainDisplay();
    void ButtonDisappear();
    
    void setDataIndex(int index);
    void setWazaData(WazaData *data);
    
protected:
    int detaIndex;
    WazaData *wazaData;
};

#endif /* defined(__waza__library__) */
