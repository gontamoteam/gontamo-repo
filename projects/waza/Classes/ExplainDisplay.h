//
//  ExplainDisplay.h
//  waza
//
//  Created by gontamo on 13/09/01.
//
//

#ifndef __waza__ExplainDisplay__
#define __waza__ExplainDisplay__

#include <iostream>
#include "cocos2d.h"
#include "WazaData.h"

class ExplainDisplay : public cocos2d::CCLayer
{
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::CCScene* scene();
    static cocos2d::CCScene* scene(WazaData *data);
    
    // a selector callback
    void menuCloseCallback(CCObject* pSender);
    
    // implement the "static node()" method manually
    CREATE_FUNC(ExplainDisplay);
    
    virtual bool ccTouchBegan(cocos2d::CCTouch* pTouch, cocos2d::CCEvent* pEvent);
    virtual void ccTouchEnded(cocos2d::CCTouch* pTouch, cocos2d::CCEvent* pEvent);
    
    //背景
    cocos2d::CCSprite* m_background;
    void showBackground();
    
    //変数初期化
    void initForVariables();
    
    //ボタン
    void LibraryButton();
    void LibraryButton3Pressed();
    void LibraryButton2Pressed();
    
    void setDataIndex(int index);
    void setWazaData(WazaData *data);
    
protected:
    int detaIndex;
    WazaData *wazaData;
};

#endif /* defined(__waza__ExplainDisplay__) */
