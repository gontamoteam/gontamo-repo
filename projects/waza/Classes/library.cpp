//
//  library.cpp
//  waza
//
//  Created by gontamo on 13/07/24.
//
//

#include "library.h"
#include "HelloWorldScene.h"
#include "ExplainDisplay.h"
#include "SimpleAudioEngine.h"
#include "CCPlaySE.h"
#include "CCParticleAction.h"

using namespace cocos2d;
using namespace CocosDenshion;

CCScene* library::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    library *layer = library::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

CCScene* library::scene(WazaData *data)
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    library *layer = library::create();
    layer->setWazaData(data);
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

void library::setDataIndex(int index)
{
    this->detaIndex = index;
}

void library::setWazaData(WazaData *data)
{
    this->wazaData = data;
}

// 初期化
bool library::init()
{
    if (!CCLayer::init())
    {
        return false;
    }
    
    // タップイベントを取得する
    setTouchEnabled(true);
    setTouchMode(kCCTouchesOneByOne);
    
    // バックキー・メニューキーイベントを取得する
    setKeypadEnabled(true);
    
    // 変数初期化
    initForVariables();
    
    // 背景表示
    showBackground();
    
    // 効果音の事前読み込み
    SimpleAudioEngine::sharedEngine()->preloadEffect("tap.mp3");
    
    //ボタン表示
    LibraryButton();
    EternalButton();
    MukaButton();
    GekiButton();
    RoyalButton();
    HaiokuButton();
    LaButton();
    HyperButton();
    CrossButton();
    UniversalButton();
//    EndButton();
//    JetButton();
//    FinalButton();
//    SpecialButton();
//    WinningButton();
//    SkyButton();
//    MarbelousButton();
    return true;
}

// 背景表示
void library::showBackground()
{
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    
    // 背景を生成
    m_background = CCSprite::create("background2.png");
    m_background->setPosition(ccp(winSize.width / 2, winSize.height / 2));
    addChild(m_background);
}

// 変数初期化
void library::initForVariables()
{
    // 乱数初期化
    srand((unsigned)time(NULL));
}
// タッチ開始イベント
bool library::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
//    CCLog("%d", this->detaIndex);
    CCLog("%s", wazaData->mName->m_sString.c_str());
    CCLog("%s", wazaData->mExplain->m_sString.c_str());
    return false;
}

// タッチ終了イベント
void library::ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent)
{
//    this->schedule(schedule_selector(HelloWorld::updatePerSec), 0.7f);
}

void library::LibraryButton()
{
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCMenuItemImage *LibraryButton = CCMenuItemImage::create(
                                                             "zukan-n.png",
                                                             "zukan-n.png",
                                                             this,
                                                             menu_selector(library::LibraryButton2Pressed));
    
	LibraryButton->setPosition(ccp(size.width*0.8 ,size.height*0.8));
    
    // create menu, it's an autorelease object
    CCMenu* LibraryMenu = CCMenu::create(LibraryButton, NULL);
    LibraryMenu->setPosition(CCPointZero);
    addChild(LibraryMenu);
}
void library::LibraryButton2Pressed()
{
    CCLog("LibraryButton2Pressed");
    CCDirector::sharedDirector()->replaceScene( HelloWorld::scene());
}

void library::ExplainDisplay()
{
    CCLog("ExplainDisplay");
    WazaData *data = new WazaData();
    data->mName = new CCString("エターナルフォースブリザード");
    data->mExplain = new CCString("エターナルフォースブリザードとは、");
    CCDirector::sharedDirector()->replaceScene( ExplainDisplay::scene(data));
}

void library::ButtonDisappear()
{
    
}
/////////////////////////////////////////
//Button
/////////////////////////////////////////
void library::EternalButton()
{
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCMenuItemImage *Button = CCMenuItemImage::create(
                                                      "matter_blue.png",
                                                      "matter_blue.png",
                                                      this,
                                                      menu_selector(library::EternalButtonPressed));
	Button->setPosition(ccp(size.width*0.2 ,size.height*0.6));
    Button->setTag(200);
    // create menu, it's an autorelease object
    CCMenu* Menu = CCMenu::create(Button, NULL);
    Menu->setPosition(CCPointZero);
    this->addChild(Menu);
}
void library::MukaButton()
{
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCMenuItemImage *Button = CCMenuItemImage::create(
                                                      "matter_red.png",
                                                      "matter_red.png",
                                                      this,
                                                      menu_selector(library::MukaButtonPressed));
	Button->setPosition(ccp(size.width*0.5 ,size.height*0.6));
    Button->setTag(201);
    // create menu, it's an autorelease object
    CCMenu* Menu = CCMenu::create(Button, NULL);
    Menu->setPosition(CCPointZero);
    this->addChild(Menu);
}

void library::GekiButton()
{
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCMenuItemImage *Button = CCMenuItemImage::create(
                                                      "matter_pink.png",
                                                      "matter_pink.png",
                                                      this,
                                                      menu_selector(library::GekiButtonPressed));
	Button->setPosition(ccp(size.width*0.8 ,size.height*0.6));
    Button->setTag(202);
    // create menu, it's an autorelease object
    CCMenu* Menu = CCMenu::create(Button, NULL);
    Menu->setPosition(CCPointZero);
    this->addChild(Menu);
}
void library::RoyalButton()
{
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCMenuItemImage *Button = CCMenuItemImage::create(
                                                      "matter_yellow.png",
                                                      "matter_yellow.png",
                                                      this,
                                                      menu_selector(library::RoyalButtonPressed));
	Button->setPosition(ccp(size.width*0.2 ,size.height*0.4));
    Button->setTag(203);
    // create menu, it's an autorelease object
    CCMenu* Menu = CCMenu::create(Button, NULL);
    Menu->setPosition(CCPointZero);
    this->addChild(Menu);
}
void library::HaiokuButton()
{
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCMenuItemImage *Button = CCMenuItemImage::create(
                                                      "matter_gray.png",
                                                      "matter_gray.png",
                                                      this,
                                                      menu_selector(library::HaiokuButtonPressed));
	Button->setPosition(ccp(size.width*0.5 ,size.height*0.4));
    Button->setTag(204);
    // create menu, it's an autorelease object
    CCMenu* Menu = CCMenu::create(Button, NULL);
    Menu->setPosition(CCPointZero);
    this->addChild(Menu);
}
void library::LaButton()
{
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCMenuItemImage *Button = CCMenuItemImage::create(
                                                      "matter_black.png",
                                                      "matter_black.png",
                                                      this,
                                                      menu_selector(library::LaButtonPressed));
	Button->setPosition(ccp(size.width*0.8 ,size.height*0.4));
    Button->setTag(205);
    // create menu, it's an autorelease object
    CCMenu* Menu = CCMenu::create(Button, NULL);
    Menu->setPosition(CCPointZero);
    this->addChild(Menu);
}
void library::HyperButton()
{
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCMenuItemImage *Button = CCMenuItemImage::create(
                                                      "matter_orange.png",
                                                      "matter_orange.png",
                                                      this,
                                                      menu_selector(library::HyperButtonPressed));
	Button->setPosition(ccp(size.width*0.2 ,size.height*0.2));
    Button->setTag(206);
    // create menu, it's an autorelease object
    CCMenu* Menu = CCMenu::create(Button, NULL);
    Menu->setPosition(CCPointZero);
    this->addChild(Menu);
}
void library::CrossButton()
{
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCMenuItemImage *Button = CCMenuItemImage::create(
                                                      "matter_purple.png",
                                                      "matter_purple.png",
                                                      this,
                                                      menu_selector(library::CrossButtonPressed));
	Button->setPosition(ccp(size.width*0.5 ,size.height*0.2));
    Button->setTag(207);
    // create menu, it's an autorelease object
    CCMenu* Menu = CCMenu::create(Button, NULL);
    Menu->setPosition(CCPointZero);
    this->addChild(Menu);
}
void library::UniversalButton()
{
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCMenuItemImage *Button = CCMenuItemImage::create(
                                                      "matter_green.png",
                                                      "matter_green.png",
                                                      this,
                                                      menu_selector(library::UniversalButtonPressed));
	Button->setPosition(ccp(size.width*0.8 ,size.height*0.2));
    Button->setTag(208);
    // create menu, it's an autorelease object
    CCMenu* Menu = CCMenu::create(Button, NULL);
    Menu->setPosition(CCPointZero);
    this->addChild(Menu);
}

//////////////////////////////////////////
//未使用
//////////////////////////////////////////
void library::EndButton(){}
void library::JetButton(){}
void library::FinalButton(){}
void library::SpecialButton(){}
void library::WinningButton(){}
void library::SkyButton(){}
void library::MarbelousButton()
{
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCMenuItemImage *Button = CCMenuItemImage::create(
                                                      "Marbelous.png",
                                                      "Marbelous.png",
                                                      this,
                                                      menu_selector(library::MarbelousButtonPressed));
	Button->setPosition(ccp(size.width*0.8 ,size.height*0.2));
    Button->setTag(210);
    // create menu, it's an autorelease object
    CCMenu* Menu = CCMenu::create(Button, NULL);
    Menu->setPosition(CCPointZero);
    this->addChild(Menu);
}
/////////////////////////////////////////////////
//Pressed
/////////////////////////////////////////////////
void library::EternalButtonPressed()
{
    ButtonNo = 201;
    ExplainDisplay();
}
void library::MukaButtonPressed(){}
void library::GekiButtonPressed(){}
void library::RoyalButtonPressed(){}
void library::HaiokuButtonPressed(){}
void library::LaButtonPressed(){}
void library::HyperButtonPressed(){}
void library::CrossButtonPressed(){}
void library::UniversalButtonPressed(){}


//////////////////////////////////////////////////
//未使用
//////////////////////////////////////////////////
void library::EndButtonPressed(){}
void library::JetButtonPressed(){}
void library::FinalButtonPressed(){}
void library::SpecialButtonPressed(){}
void library::WinningButtonPressed(){}
void library::SkyButtonPressed(){}
void library::MarbelousButtonPressed()
{
    ButtonNo = 210;
    ExplainDisplay();
}