#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "Config.h"
#include "../proj.android/src/InAppBillingManager.h"
#define PNG_START "jumon.png"
#define PNG_STOP "stop.png"

class HelloWorld : public cocos2d::CCLayer, public InAppBillingDelegate
{
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::CCScene* scene();
    
    // a selector callback
    void menuCloseCallback(CCObject* pSender);
    
    // implement the "static node()" method manually
    CREATE_FUNC(HelloWorld);

    int m_iSec;
    int index;
    void updatePerSec();

    bool m_animating;

    virtual bool ccTouchBegan(cocos2d::CCTouch* pTouch, cocos2d::CCEvent* pEvent);
    virtual void ccTouchEnded(cocos2d::CCTouch* pTouch, cocos2d::CCEvent* pEvent);
    
    //スタートボタン
    void startButtonPressed(cocos2d::CCObject* pSender2);
    void startButton();
    bool leftslotOn;
    bool centerslotOn;
    bool rightslotOn;
    bool reelOn;
    cocos2d::CCLabelTTF *startlabel;

    //ストップボタン
    void StopButtonPressed(cocos2d::CCObject* pSender3);
    void stopButton();
    //ブルーキャラ
    void blueButtonPressed(cocos2d::CCObject* blueSender);
    void blueButton();
    //イエローキャラ
    void yellowButtonPressed(cocos2d::CCObject* yellowSender);
    void yellowButton();
    //レッドキャラ
    void redButtonPressed(cocos2d::CCObject* redSender);
    void redButton();
    //敵キャラ
    void enemyButton();
    bool enemyActionflg;
    void enemyAction();
    void actionFinished();
    void ActionDone();
    void EscapeAction();
    void DefeatAction();
    void enemyArrival();
    //広告;
    void AdButton();
    void AdButtonPressed();
    //図鑑
    void LibraryButton();
    void LibraryButtonPressed();
    //エフェクト
    void EffectButton();
    void EffectButtonPressed();
    bool EffectDoneflg;
    void EffectDone();
    
    void ExplosionDone();
    void SnowDone();
    void RainDone();
    void SmokeDone();
    void FireDone();
    void FireworksDone();
    void GalaxyDone();
    void MeteorDone();
    void SpiralDone();
    void SunDone();
    
    //スロット当たり判定
    int indexfukusi;
    int indexkeiyosi;
    int indexmeisi;
    bool totalAttackFlg;
    //背景
    cocos2d::CCSprite* m_background;
    void showBackground();
    
    //パラメータ表示
    int HissatuPoint;
    void HissatuPointDisplay();
    void HissatuPointUpdate();
    int ExpPoint;
    void ExpPointDisplay();
    void ExpPointUpdate();
    int moneyPoint;
    void moneyPointDisplay();
    void moneyPointUpdate();
};

#endif // __HELLOWORLD_SCENE_H__
