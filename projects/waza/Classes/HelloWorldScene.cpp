#include "GameScene.h"
#include "SimpleAudioEngine.h"
#include "BlockSprite.h"
#include "CCPlaySE.h"
#include "HelloWorldScene.h"
#include <time.h>
#include "CCParticleAction.h"
#include "library.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "InterfaceJNI.h"
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
//#include "../proj.ios/AppController.h"
#endif
USING_NS_CC;

CCScene* HelloWorld::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    HelloWorld *layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{

//背景表示
    showBackground();

//初期化
    leftslotOn = false;
    centerslotOn = false;
    rightslotOn = false;
    reelOn = false;
    enemyActionflg = false;
    EffectDoneflg = false;
    
    totalAttackFlg = false;
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }
    
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    CCMenuItemImage *pCloseItem = CCMenuItemImage::create(
                                        "CloseNormal.png",
                                        "CloseSelected.png",
                                        this,
                                        menu_selector(HelloWorld::menuCloseCallback));
    
	pCloseItem->setPosition(ccp(origin.x + visibleSize.width - pCloseItem->getContentSize().width/2 ,
                                origin.y + pCloseItem->getContentSize().height/2));

    // create menu, it's an autorelease object
    CCMenu* pMenu = CCMenu::create(pCloseItem, NULL);
    pMenu->setPosition(CCPointZero);
    this->addChild(pMenu, 1);


//ボタン表示
    startButton();
    blueButton();
    yellowButton();
    redButton();
    AdButton();
    LibraryButton();
    EffectButton();
    
//パラメータ表示
    HissatuPointDisplay();
    ExpPointDisplay();
    moneyPointDisplay();
    
    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label
    
//    CCLabelTTF* pLabel = CCLabelTTF::create("Gontamo World", "Arial", 24);
//    
//    // position the label on the center of the screen
//    pLabel->setPosition(ccp(origin.x + visibleSize.width/2,
//                            origin.y + visibleSize.height - pLabel->getContentSize().height));
//
//    // add the label as a child to this layer
//    this->addChild(pLabel, 1);

//    // add "HelloWorld" splash screen"
//    CCSprite* pSprite = CCSprite::create("HelloWorld.png");
//
//    // position the sprite on the center of the screen
//    pSprite->setPosition(ccp(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
//
//    // add the sprite as a child to this layer
//    this->addChild(pSprite, 0);

    
//waza表示
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCLabelTTF* pfukusi = CCLabelTTF::create("ブルータス", "Thonburi", 34);
    CCLabelTTF* pkeiyosi = CCLabelTTF::create("イエローラ", "Thonburi", 34);
    CCLabelTTF* pmeisi = CCLabelTTF::create("ラドレッド", "Thonburi", 34);
    pfukusi->setPosition(ccp(size.width*0.2, size.height*0.3));
    pkeiyosi->setPosition(ccp(size.width/2, size.height*0.3));
    pmeisi->setPosition(ccp(size.width*0.8, size.height*0.3));
    this->addChild(pfukusi);
    this->addChild(pkeiyosi);
    this->addChild(pmeisi);
    pfukusi->setTag(88);
    pkeiyosi->setTag(44);
    pmeisi->setTag(31);
    index = 1;
//秒数表示
    CCLabelTTF* pSec = CCLabelTTF::create("0秒", "Thonburi", 34);
    pSec->setPosition(ccp(size.width*0.08, size.height*0.2));
    this->addChild(pSec);
    pSec->setTag(10);
//秒数初期化
    m_iSec = 0;

//効果音
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("fire.mp3");
    return true;
}

// 背景表示
void HelloWorld::showBackground()
{
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    
    // 背景を生成
    m_background = CCSprite::create("background2.png");
    m_background->setPosition(ccp(winSize.width / 2, winSize.height / 2));
    addChild(m_background);
}

// ひっさつポイント表示
void HelloWorld::HissatuPointDisplay()
{
    HissatuPoint = 10;
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCLabelTTF *HissatuPointLabel = CCLabelTTF::create("ひっさつエネルギー 10/10", "Thonburi", 34);
    HissatuPointLabel->setTag(100);
    HissatuPointLabel->setPosition(ccp(size.width/3 ,size.height*0.05));
	this->addChild(HissatuPointLabel);
}
//ひっさつポイント更新
void HelloWorld::HissatuPointUpdate()
{
    HissatuPoint -= 1;
    CCLabelTTF* HissatuPointLabel = (CCLabelTTF*)this->getChildByTag(100);
    HissatuPointLabel->setString(CCString::createWithFormat("ひっさつエネルギー %2d/10", HissatuPoint)->getCString());
}

//お金ポイント表示
void HelloWorld::moneyPointDisplay()
{
    moneyPoint = 100;
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCLabelTTF *moneyPointLabel = CCLabelTTF::create("100ワザー", "Thonburi", 34);
    moneyPointLabel->setTag(101);
    moneyPointLabel->setPosition(ccp(size.width*0.2 ,size.height*0.58));
	this->addChild(moneyPointLabel);
    
    CCSprite *coinImage = CCSprite::create("coin.png");
    coinImage->setPosition(ccp(size.width * 0.2, size.height / 2));
    addChild(coinImage);
}

//お金ポイント更新
void HelloWorld::moneyPointUpdate()
{
    if (totalAttackFlg) {
        moneyPoint += 2;
    }
    moneyPoint += 1;
    CCLabelTTF* moneyPointLabel = (CCLabelTTF*)this->getChildByTag(101);
    moneyPointLabel->setString(CCString::createWithFormat("お金\n%3dワザー", moneyPoint)->getCString());
}

//経験値
void HelloWorld::ExpPointDisplay()
{
    ExpPoint = 0;
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCLabelTTF *ExpPointLabel = CCLabelTTF::create("経験値　0", "Thonburi", 34);
    ExpPointLabel->setTag(102);
    ExpPointLabel->setPosition(ccp(size.width*0.7 ,size.height*0.05));
	this->addChild(ExpPointLabel);
}

//〇〇秒ごとにupdateされる
void HelloWorld::updatePerSec() {
    //秒数
    m_iSec++;
    CCLabelTTF* pLabel = (CCLabelTTF*)this->getChildByTag(10);
    pLabel->setString(CCString::createWithFormat("%d秒", m_iSec)->getCString());

//読み込み
    CCArray *items = CCArray::createWithContentsOfFile("waza.plist");

    CCObject *dic= items->objectAtIndex(index);
    
    CCObject *fukusi = ((CCDictionary*)(dic))->objectForKey("fukusi");
    CCObject *keiyosi = ((CCDictionary*)(dic))->objectForKey("keiyosi");
    CCObject *meisi = ((CCDictionary*)(dic))->objectForKey("meisi");
    CCString *fukusi2 = (CCString *)(fukusi);
    CCString *keiyosi2 = (CCString *)(keiyosi);
    CCString *meisi2 = (CCString *)(meisi);

//    CCLOG("%dfukusi:%s",index, fukusi2->getCString());
//    CCLOG("%dkeiyosi:%s",index, ((CCString *)(keiyosi))->getCString());
//    CCLOG("%dmeisi:%s", index,((CCString *)(meisi))->getCString());
    if(leftslotOn)
    {
        CCLabelTTF* pfukusi = (CCLabelTTF*)this->getChildByTag(88);
        pfukusi->setString(fukusi2->getCString());
        indexfukusi = index;
    }
    if(centerslotOn)
    {
        CCLabelTTF* pkeiyosi = (CCLabelTTF*)this->getChildByTag(44);
        pkeiyosi->setString(keiyosi2->getCString());
        indexkeiyosi = index;
    }
    if(rightslotOn)
    {
        CCLabelTTF* pmeisi = (CCLabelTTF*)this->getChildByTag(31);
        pmeisi->setString(meisi2->getCString());
        indexmeisi = index;
    }
    if(reelOn)
    {
        index++;
    }
    if(index>=15){index=1;}
    
//すべてのリールが止まった時
    if(!leftslotOn && !centerslotOn&& !rightslotOn && reelOn)
    {
        enemyActionflg = true;
        reelOn = false;
        //そろう判定
        if (indexfukusi == indexkeiyosi && indexkeiyosi == indexmeisi) {
            if (indexfukusi == 1) {
                FireDone();
                totalAttackFlg = true;
            }
            if (indexfukusi == 2) {
                ExplosionDone();
                totalAttackFlg = true;
            }
        }
        else
        {
            totalAttackFlg = false;
            EffectDone();
        }
    }
}

//電源ボタンを押したときの動き
void HelloWorld::menuCloseCallback(CCObject* pSender)
{
    CCDirector::sharedDirector()->end();
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

// タッチ開始イベント
bool HelloWorld::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
    // アニメーション中はタップ処理を受け付けない
    return !m_animating;
}

// タッチ終了イベント
void HelloWorld::ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent)
{
    this->schedule(schedule_selector(HelloWorld::updatePerSec), 0.7f);
}



//　スタートボタン作成
void HelloWorld::startButton()
{
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCMenuItemImage *startButton = CCMenuItemImage::create(
                                                           PNG_START,
                                                           PNG_START,
                                                           this,
                                                           menu_selector(HelloWorld::startButtonPressed));
    startButton->setPosition(ccp(size.width/2 ,size.height*0.5));
    
    // create menu, it's an autorelease object
    CCMenu* pMenu3 = CCMenu::create(startButton, NULL);
    pMenu3->setPosition(CCPointZero);
    pMenu3->setTag(11);

    this->addChild(pMenu3);
    CCLabelTTF *startButtonlabel = CCLabelTTF::create("ひっさつ！", "Thonburi", 64);
    startButtonlabel->setPosition(ccp(size.width/2 ,size.height*0.5));
    startButtonlabel->setTag(111);
	this->addChild(startButtonlabel);
}

//　スタートボタンタップ時の処理
void HelloWorld::startButtonPressed(cocos2d::CCObject* pSender2)
{
    this->schedule(schedule_selector(HelloWorld::updatePerSec), 0.5f);
    leftslotOn = true;
    centerslotOn = true;
    rightslotOn = true;
    reelOn = true;
    enemyButton();
    enemyArrival();
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("shakin.mp3");

    //ひっさつポイントを減らす
    HissatuPointUpdate();
    
    //スタートボタンを使えなくする
    CCMenu *pMenu3 = (CCMenu*)this->getChildByTag(11);
    pMenu3->setEnabled(false);
    CCLabelTTF *startButtonlabel = (CCLabelTTF *)this->getChildByTag(111);
    startButtonlabel->setVisible(false);
}

//ブルーキャラ
void HelloWorld::blueButton()
{
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCMenuItemImage *blueButton = CCMenuItemImage::create(
                                                           "blue-n.png",
                                                           "blue-n.png",
                                                           this,
                                                           menu_selector(HelloWorld::blueButtonPressed));
	blueButton->setPosition(ccp(size.width*0.2 ,size.height*0.2));
    
    // create menu, it's an autorelease object
    CCMenu* blueMenu = CCMenu::create(blueButton, NULL);
    blueMenu->setPosition(CCPointZero);
    this->addChild(blueMenu);
}
void HelloWorld::blueButtonPressed(cocos2d::CCObject* blueSender)
{
    leftslotOn = false;
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("tap.mp3");
}
//イエローキャラ
void HelloWorld::yellowButton()
{
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCMenuItemImage *yellowButton = CCMenuItemImage::create(
                                                          "yellow-n.png",
                                                          "yellow-n.png",
                                                          this,
                                                          menu_selector(HelloWorld::yellowButtonPressed));
    
	yellowButton->setPosition(ccp(size.width*0.5 ,size.height*0.2));
    
    // create menu, it's an autorelease object
    CCMenu* yellowMenu = CCMenu::create(yellowButton, NULL);
    yellowMenu->setPosition(CCPointZero);
    this->addChild(yellowMenu);
}
void HelloWorld::yellowButtonPressed(cocos2d::CCObject* yellowSender)
{
    centerslotOn = false;
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("tap.mp3");

}

//レッドキャラ
void HelloWorld::redButton()
{
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCMenuItemImage *redButton = CCMenuItemImage::create(
                                                            "red-n.png",
                                                            "red-n.png",
                                                            this,
                                                            menu_selector(HelloWorld::redButtonPressed));
    
	redButton->setPosition(ccp(size.width*0.8 ,size.height*0.2));
    
    // create menu, it's an autorelease object
    CCMenu* redMenu = CCMenu::create(redButton, NULL);
    redMenu->setPosition(CCPointZero);
    this->addChild(redMenu);
}

void HelloWorld::redButtonPressed(cocos2d::CCObject* redSender)
{
    rightslotOn = false;
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("tap.mp3");

}
void HelloWorld::enemyButton()
{
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCSprite *enemySprite = CCSprite::create("devil.png");
	enemySprite->setPosition(ccp(size.width*0.5 ,size.height*1.2));
    enemySprite->setTag(1);
    addChild(enemySprite);
    
    CCSprite *enemySpriteleft = CCSprite::create("devil.png");
	enemySpriteleft->setPosition(ccp(size.width*0.4 ,size.height*1.2));
    enemySpriteleft->setTag(2);
    addChild(enemySpriteleft);
    
    CCSprite *enemySpriteright = CCSprite::create("devil.png");
	enemySpriteright->setPosition(ccp(size.width*0.6 ,size.height*1.2));
    enemySpriteright->setTag(3);
    addChild(enemySpriteright);
    
}
void HelloWorld::enemyArrival()
{
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCSprite *enemySprite = (CCSprite *)this->getChildByTag(1);
    CCSprite *enemySpriteleft = (CCSprite *)this->getChildByTag(2);
    CCSprite *enemySpriteright = (CCSprite *)this->getChildByTag(3);
    
    CCMoveTo* move = CCMoveTo::create(0.4f, ccp(size.width*0.5 ,size.height*0.8));
    CCMoveTo* moveleft = CCMoveTo::create(0.4f, ccp(size.width*0.4 ,size.height*0.8));
    CCMoveTo* moveright = CCMoveTo::create(0.4f, ccp(size.width*0.6 ,size.height*0.8));
    
    enemySprite -> runAction(move);
    enemySpriteleft->runAction(moveleft);
    enemySpriteright->runAction(moveright);
}
void HelloWorld::enemyAction()
{
    enemyActionflg = false;
    CCLOG("enemyAction");
//    reelOn = false;
    CCRotateBy* action = CCRotateBy::create(0.3f,360);
    CCRotateBy* action2 = CCRotateBy::create(0.3f,360);
    CCRotateBy* action3 = CCRotateBy::create(0.3f,360);

    CCSprite *enemySprite = (CCSprite *)this->getChildByTag(1);
    CCSprite *enemySpriteleft = (CCSprite *)this->getChildByTag(2);
    CCSprite *enemySpriteright = (CCSprite *)this->getChildByTag(3);

    CCCallFuncN *func = CCCallFuncN::create(this, callfuncN_selector(HelloWorld::ActionDone));
    CCCallFuncN *func2 = CCCallFuncN::create(this, callfuncN_selector(HelloWorld::EscapeAction));
    CCCallFuncN *func3 = CCCallFuncN::create(this, callfuncN_selector(HelloWorld::DefeatAction));
    
    if (totalAttackFlg)
    {
        CCLog("func3");
        enemySpriteleft->runAction(CCSequence::create(action2, func3, NULL));
        enemySpriteright->runAction(CCSequence::create(action3, func3, NULL));

    }
    else
    {
        enemySpriteleft->runAction(CCSequence::create(func2, NULL));
        enemySpriteright->runAction(CCSequence::create(func2, NULL));
    }
    
    enemySprite->runAction(CCSequence::create(action, func, NULL));

    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("enemyAction.mp3");
}

void HelloWorld::ActionDone()
{
    CCSprite *enemySprite = (CCSprite *)this->getChildByTag(1);
    CCSprite *enemySpriteleft = (CCSprite *)this->getChildByTag(2);
    CCSprite *enemySpriteright = (CCSprite *)this->getChildByTag(3);

    removeChild(enemySprite, true);
    removeChild(enemySpriteleft, true);
    removeChild(enemySpriteright, true);
    
    //スタートボタンを利用可能にする
    CCMenu *pMenu3 = (CCMenu*)this->getChildByTag(11);
    pMenu3->setEnabled(true);
    CCLabelTTF *startButtonlabel = (CCLabelTTF *)this->getChildByTag(111);
    startButtonlabel->setVisible(true);
    
    //お金ポイントを増やす
    moneyPointUpdate();
}

void HelloWorld::EscapeAction()
{
    CCLog("EscapeAction");
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCSprite *enemySpriteleft = (CCSprite *)this->getChildByTag(2);
    CCSprite *enemySpriteright = (CCSprite *)this->getChildByTag(3);
    CCMoveTo* moveleft = CCMoveTo::create(1.0f, ccp(size.width*0.4 ,size.height*1.2));
    CCMoveTo* moveright = CCMoveTo::create(1.0f, ccp(size.width*0.6 ,size.height*1.2));
    enemySpriteleft->runAction(moveleft);
    enemySpriteright->runAction(moveright);
}

void HelloWorld::DefeatAction()
{
    CCLog("Defeat Action");
}

void HelloWorld::AdButton()
{
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCMenuItemImage *AdButton = CCMenuItemImage::create(
                                                         "akita.png",
                                                         "akita.png",
                                                         this,
                                                         menu_selector(HelloWorld::AdButtonPressed));
    
	AdButton->setPosition(ccp(size.width*0.2 ,size.height*0.8));
    
    // create menu, it's an autorelease object
    CCMenu* AdMenu = CCMenu::create(AdButton, NULL);
    AdMenu->setPosition(CCPointZero);
    addChild(AdMenu);
}

void HelloWorld::AdButtonPressed()
{
    CCLog("AdButtonPressed");
//    InAppBillingManager::setDelegate(this);
//    InAppBillingManager::purchaseForItemId("abcdefg");
//    const char* version = getAppVersion();
    // jniはandroid用専用なので、#ifで読み分ける
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    InterfaceJNI::func1();
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
//    RootViewController::func1();
#endif
}

void HelloWorld::LibraryButton()
{
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCMenuItemImage *LibraryButton = CCMenuItemImage::create(
                                                        "zukan-n.png",
                                                        "zukan-n.png",
                                                        this,
                                                        menu_selector(HelloWorld::LibraryButtonPressed));
    
	LibraryButton->setPosition(ccp(size.width*0.8 ,size.height*0.8));
    
    // create menu, it's an autorelease object
    CCMenu* LibraryMenu = CCMenu::create(LibraryButton, NULL);
    LibraryMenu->setPosition(CCPointZero);
    addChild(LibraryMenu);
}
void HelloWorld::LibraryButtonPressed()
{
    CCLog("LibraryButtonPressed");
//    CCScene *libScene = library::scene();
//    library *lib = (library *)(libScene->getChildren()->objectAtIndex(0));
//    lib->setDataIndex(100);
    WazaData *data = new WazaData();
    data->mName = new CCString("Name: Hello World -> library");
    data->mExplain = new CCString("Explain: Hello World -> library");
    CCDirector::sharedDirector()->replaceScene(library::scene(data));

}

void HelloWorld::EffectButton()
{
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCMenuItemImage *EffectButton = CCMenuItemImage::create(
                                                             "zukan-a.png",
                                                             "zukan-a.png",
                                                             this,
                                                             menu_selector(HelloWorld::EffectButtonPressed));
    
	EffectButton->setPosition(ccp(size.width*0.8 ,size.height*0.5));
    
    // create menu, it's an autorelease object
    CCMenu* EffectMenu = CCMenu::create(EffectButton, NULL);
    EffectMenu->setPosition(CCPointZero);
    addChild(EffectMenu);
}
void HelloWorld::EffectButtonPressed()
{
    CCLog("EffectButtonPressed");
    ExplosionDone();
}

void HelloWorld::EffectDone()
{
    CCParticleSmoke* pParticle = CCParticleSmoke::createWithTotalParticles(100);
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    pParticle->runAction(
                    CCSequence::create(
                                       CCParticleStop::create(),
                                       CCDelayTime::create(0.0f),
                                       CCParticleReset::create(),
                                       CCParticleSrcPosTo::create(0.0f, ccp(size.width*-0.0,size.height*0.8)),
                                       CCParticleSrcPosTo::create(0.3f, ccp(size.width*0.0,size.height*0.8)),
                                       CCParticleStop::create(),
                                       NULL
                                       )
                    );
    addChild(pParticle);
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("normal.mp3");
    this->scheduleOnce(schedule_selector(HelloWorld::enemyAction), 1.0f);
}
void HelloWorld::ExplosionDone()
{
    CCParticleExplosion* pParticle = CCParticleExplosion::createWithTotalParticles(100);
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    pParticle->runAction(
                         CCSequence::create(
                                            CCParticleStop::create(),
                                            CCDelayTime::create(0.0f),
                                            CCParticleReset::create(),
                                            CCParticleSrcPosTo::create(0.0f, ccp(size.width*-0.2,size.height*0.2)),
                                            CCParticleSrcPosTo::create(0.3f, ccp(size.width*0.8,size.height*0.2)),
                                            CCParticleStop::create(),
                                            NULL
                                            )
                         );
    addChild(pParticle);
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("explosion.mp3");
    CCSprite *enemySprite = (CCSprite *)this->getChildByTag(1);
    if(enemySprite)
    {
        this->scheduleOnce(schedule_selector(HelloWorld::enemyAction), 1.0f);
    }
        CCLog("ExplosionDone");
}

void HelloWorld::SnowDone(){}
void HelloWorld::RainDone(){}
void HelloWorld::SmokeDone(){}
void HelloWorld::FireDone()
{
    CCParticleSun* pParticle = CCParticleSun::createWithTotalParticles(100);
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    pParticle->runAction(
                         CCSequence::create(
                                            CCParticleStop::create(),
                                            CCDelayTime::create(0.0f),
                                            CCParticleReset::create(),
                                            CCParticleSrcPosTo::create(0.0f, ccp(size.width*-0.2,size.height*0.2)),
                                            CCParticleSrcPosTo::create(0.3f, ccp(size.width*0.2,size.height*0.2)),
                                            CCParticleStop::create(),
                                            NULL
                                            )
                         );
    addChild(pParticle);
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("fire.mp3");
    CCSprite *enemySprite = (CCSprite *)this->getChildByTag(1);
    if(enemySprite)
    {
        this->scheduleOnce(schedule_selector(HelloWorld::enemyAction), 1.0f);
    }
    CCLog("FireDone");
}
void HelloWorld::FireworksDone(){}
void HelloWorld::GalaxyDone(){}
void HelloWorld::MeteorDone(){}
void HelloWorld::SpiralDone(){}
void HelloWorld::SunDone(){}
