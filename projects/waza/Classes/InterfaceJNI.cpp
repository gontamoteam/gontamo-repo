#include "InterfaceJNI.h"
#include "platform/android/jni/JniHelper.h"
#include <jni.h>
#include <android/log.h>

// 呼び出すメソッドが書かれているパッケージ名とjavaクラス名
#define  CLASS_NAME "jp/co/gontamo/jniTest2"

void InterfaceJNI::func1()
{
    JniMethodInfo t;
    // クラス名とメソッド名を指定します。
    if (JniHelper::getMethodInfo(t, CLASS_NAME, "func1", "()V")) {
        // voidなので、CallStaticVoidMethodで呼ぶ
        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        // 解放
        t.env->DeleteLocalRef(t.classID);
    }
}