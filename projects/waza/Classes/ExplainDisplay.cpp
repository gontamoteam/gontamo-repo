//
//  ExplainDisplay.cpp
//  waza
//
//  Created by gontamo on 13/09/01.
//
//

#include "ExplainDisplay.h"
#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "CCPlaySE.h"
#include "CCParticleAction.h"

using namespace cocos2d;
using namespace CocosDenshion;

CCScene* ExplainDisplay::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    ExplainDisplay *layer = ExplainDisplay::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

CCScene* ExplainDisplay::scene(WazaData *data)
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    ExplainDisplay *layer = ExplainDisplay::create();
    layer->setWazaData(data);
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

void ExplainDisplay::setDataIndex(int index)
{
    this->detaIndex = index;
}

void ExplainDisplay::setWazaData(WazaData *data)
{
    this->wazaData = data;
}

// 初期化
bool ExplainDisplay::init()
{
    if (!CCLayer::init())
    {
        return false;
    }
    
    // タップイベントを取得する
    setTouchEnabled(true);
    setTouchMode(kCCTouchesOneByOne);
    
    // バックキー・メニューキーイベントを取得する
    setKeypadEnabled(true);
    
    // 変数初期化
    initForVariables();
    
    // 背景表示
    showBackground();
    
    // 効果音の事前読み込み
    SimpleAudioEngine::sharedEngine()->preloadEffect("tap.mp3");
    
    //ボタン表示
    LibraryButton();
    return true;
}

// 背景表示
void ExplainDisplay::showBackground()
{
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    
    // 背景を生成
    m_background = CCSprite::create("background2.png");
    m_background->setPosition(ccp(winSize.width / 2, winSize.height / 2));
    addChild(m_background);
}

// 変数初期化
void ExplainDisplay::initForVariables()
{
    // 乱数初期化
    srand((unsigned)time(NULL));
}
// タッチ開始イベント
bool ExplainDisplay::ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent)
{
    //    CCLog("%d", this->detaIndex);
    CCLog("%s", wazaData->mName->m_sString.c_str());
    CCLog("%s", wazaData->mExplain->m_sString.c_str());
    return false;
}

// タッチ終了イベント
void ExplainDisplay::ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent)
{
    //    this->schedule(schedule_selector(HelloWorld::updatePerSec), 0.7f);
}

void ExplainDisplay::LibraryButton()
{
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCMenuItemImage *LibraryButton = CCMenuItemImage::create(
                                                             "zukan-n.png",
                                                             "zukan-n.png",
                                                             this,
                                                             menu_selector(ExplainDisplay::LibraryButton3Pressed));
    
	LibraryButton->setPosition(ccp(size.width*0.8 ,size.height*0.8));
    
    // create menu, it's an autorelease object
    CCMenu* LibraryMenu = CCMenu::create(LibraryButton, NULL);
    LibraryMenu->setPosition(CCPointZero);
    addChild(LibraryMenu);
}
void ExplainDisplay::LibraryButton3Pressed()
{
    CCLog("LibraryButton3Pressed");
    //    CCScene *libScene = library::scene();
    //    library *lib = (library *)(libScene->getChildren()->objectAtIndex(0));
    //    lib->setDataIndex(100);
//    WazaData *data = new WazaData();
//    data->mName = "エターナルフォースなんとか";
//    data->mExplain = "うんこ";
    CCDirector::sharedDirector()->replaceScene(HelloWorld::scene());
}

