//
//  CCDatabaseManager.h
//  waza
//
//  Created by ugabugab on 9/1/13.
//
//

#ifndef __waza__CCDatabaseManager__
#define __waza__CCDatabaseManager__

#include "cocos2d.h"

using namespace cocos2d;

class CCDatabaseManager : public CCObject
{
private:
    static CCDatabaseManager *sDatabaseManager; // singleton instance
    CCString *getDatabaseFilePath();
    
public:
    CCDatabaseManager();
    virtual ~CCDatabaseManager();
    
    // get instance of CCDatabaseManager
    static CCDatabaseManager *getInstance()
    {
        if (sDatabaseManager == NULL)
        {
            sDatabaseManager = new CCDatabaseManager();
            sDatabaseManager->initialize();
        }
        return sDatabaseManager;
    }
    
    void initialize();
    void terminate();
    
    // select
    CCArray *selectAllWazaDatas();
    
    // inset
    
    // delete
};

#endif /* defined(__waza__CCDatabaseManager__) */
