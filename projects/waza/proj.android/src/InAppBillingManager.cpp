#include "InAppBillingManager.h"
#include "InAppBillingJni.h"

static InAppBillingDelegate *m_delegate;

void InAppBillingManager::setDelegate(InAppBillingDelegate* pDelegate)
{
    m_delegate = pDelegate;
}

void InAppBillingManager::purchaseForItemId(std::string itemId)
{
	purchaseForItemIdJni(itemId.c_str());
}

void InAppBillingManager::onSuccess(std::string pItemId)
{
	if (m_delegate)
		m_delegate->onSuccess(pItemId);
}

void InAppBillingManager::onCancel()
{
	if (m_delegate)
		m_delegate->onCancel();
}
