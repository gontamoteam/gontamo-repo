#include "InAppBillingJni.h"
#include "platform/android/jni/JniHelper.h"
#include <jni.h>
#include "InAppBillingManager.h"

extern "C"
{
	void purchaseForItemIdJni(const char* itemId)
	{
		cocos2d::JniMethodInfo t;
		if (cocos2d::JniHelper::getStaticMethodInfo(t
			, "jp/co/gontamo/InAppBillingTest"
			, "purchaseForItemId"
			, "(Ljava/lang/String;)V"))
		{
			int a = 1;
			printf("%d",a);
			jstring stringArg = t.env->NewStringUTF(itemId);
			t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg);
			t.env->DeleteLocalRef(stringArg);
			t.env->DeleteLocalRef(t.classID);
		}
	}

    void JNICALL Java_net_tks2_inappbilling_InAppBillingTest_onSuccess(JNIEnv* env, jobject thiz, jstring itemId) {
        const char* pszItemId = env->GetStringUTFChars(itemId, NULL);
        InAppBillingManager::onSuccess(pszItemId);
        env->ReleaseStringUTFChars(itemId, pszItemId);
    }

    void JNICALL Java_net_tks2_inappbilling_InAppBillingTest_onCancel(JNIEnv* env, jobject thiz) {
        InAppBillingManager::onCancel();
    }
}
