#ifndef __INAPP_BILLING_JNI_H__
#define __INAPP_BILLING_JNI_H__

extern "C"

{
	extern void purchaseForItemIdJni(const char* itemId);
}

#endif // __INAPP_BILLING_JNI_H__
