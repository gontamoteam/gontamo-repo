#import "RootViewController.h"


@implementation RootViewController

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
        
//        // GFIconControllerの初期化
//        gfIconController = [[GFIconController alloc] init];
//        
//        // アイコンの自動更新間隔を指定（デフォルトで30秒／最短10秒）
//        [gfIconController setRefreshTiming:20];
//        
//        // アイコン下のアプリ名テキストの色をUIColorで指定出来ます（デフォルト黒）
//        [gfIconController setAppNameColor:[UIColor redColor]];
//        
//        // アイコンの配置位置を設定（1個〜20個まで設置出来ます）
//        {
//            GFIconView *iconView = [[[GFIconView alloc] initWithFrame:CGRectMake(100,100,100,100)] autorelease];
//            [gfIconController addIconView:iconView];
//            [self.view addSubview:iconView];
//        }
    }
    return self;
}




// Implement loadView to create a view hierarchy programmatically, without using a nib.
//- (void)loadView {
//}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
//- (void)viewDidLoad {
//    [super viewDidLoad];
//}

//- (void)viewDidAppear:(BOOL)animated
//{
//    [super viewDidAppear:animated];
//    
//    // アイコン広告の表示
////    [gfIconController loadAd:@"1824"];
//}

//- (void)viewWillDisappear:(BOOL)animated
//{
    // アイコン広告の自動更新を停止
//    [gfIconController stopAd];
//    [super viewWillDisappear:animated];
//}

// Override to allow orientations other than the default portrait orientation.
// This method is deprecated on ios6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return UIInterfaceOrientationIsLandscape( interfaceOrientation );
}

// For ios6, use supportedInterfaceOrientations & shouldAutorotate instead
- (NSUInteger) supportedInterfaceOrientations{
#ifdef __IPHONE_6_0
    return UIInterfaceOrientationMaskAllButUpsideDown;
#endif
}

- (BOOL) shouldAutorotate {
    return YES;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
